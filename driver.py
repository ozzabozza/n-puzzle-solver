
import sys #needed for argv use
import io
import math
from classes import Solver
from collections import deque
import pdb
import time



#PROGRAM STARTUP CHECKS
if len(sys.argv) < 3:
	print "Missing arguments, type: python driver.py <method> <board>"
	exit()

numbers = sys.argv[2]
initialstate = [int(x) for x in numbers.split(',')] #make list of numbers

if not math.sqrt(len(initialstate)).is_integer():
	print "Invalid board size, board should be a square"
	exit()

#PROGRAM MAIN LOGIC
if (sys.argv[1] == 'bfs'):
	print "we use bfs with follwing numbers:"
	print initialstate
	solution = Solver(state = initialstate)
	solution.BFS()
elif (sys.argv[1] == 'dfs'):
	print "we use dfs with follwing numbers:"
	print initialstate
	solution = Solver(state = initialstate)
	solution.DFS()
elif (sys.argv[1] == 'ast'):
	print "we use ast with follwing numbers:"
	print initialstate
	solution = Solver(state = initialstate)
	solution.AST()
elif (sys.argv[1] == 'ida'):
	print "we use ida with follwing numbers:"
	print initialstate
	solution = Solver(state = initialstate)
	solution.IDA()
else:
	print "Wrong method: choose bfs, dfs, ast or ida"
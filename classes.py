import math
from collections import deque
from sets import Set
import pdb
import time
import heapq
import resource 
import sys #needed for argv use

#PROGRAM CLASSES
class State(object): #state of the game class used in solver class
	def __init__(self, state=(), parent_node = None , cost_till_now = '000', node_type = '000'):
		self.state = state
		self.parent_node = parent_node
		self.node_type = node_type
		self.childs = deque()
		self.search_depth = 0
		self.heuristic = 0

class Solver(object): #solver class
	def __init__(self, state = []):
		self.initialstate = State(state)
		self.size = int(math.sqrt(len(self.initialstate.state))) #calculate first node length, maybe check input with it
		self.start_node = ','.join(str(v) for v in self.initialstate.state)
		self.frontier_check = set()
		self.frontier_check.add(self.start_node)
		self.visited = set()
		self.path_to_goal= []
		self.nodes_visited = 0
		self.cost_of_path = 0
		self.nodes_expanded = 0
		self.fringe_size = 0		
		self.max_fringe_size = 5 	
		self.search_depth = 0	
		self.max_search_depth = 0
		self.max_ram_usage = 9
		self.start_time = time.time()

	def BFS(self):
		self.frontier = deque() #Add first node to the right side of the deque
		self.frontier.append(self.initialstate)
		while self.frontier:
			if len(self.frontier) > self.max_fringe_size:
				self.max_fringe_size = len(self.frontier)
			current_node = self.frontier.popleft()	#1.remove state object from deque and call it current_node
			length = len(current_node.state)
			n = math.sqrt(length)

			if self.is_goal_node(current_node) == True: #2.retrive information and create output fill in seperate method of solver class
				print "Solution found"
				self.write_output_file(current_node)
				self.visited.add(tuple(current_node.state)) #append current node to visited
				exit()

			self.get_children(current_node, n)	#3.expand node and put in frontier only if not yet in frontier and not yet visited     
			# kan dit in een aparte method?
			self.nodes_expanded += 1
			if current_node.search_depth > self.max_search_depth:
				self.max_search_depth = current_node.search_depth
			
			visited_state = ','.join(str(v) for v in current_node.state)#append current node to visited
			self.visited.add(visited_state)

			self.visited.add(tuple(current_node.state))
			self.nodes_visited = self.nodes_visited +1
			
			for child in current_node.childs:
				string_state = ','.join(str(v) for v in child.state)
				if string_state not in self.visited and string_state not in self.frontier_check:    #not any(x.state == child.state for x in self.frontier): 
					self.frontier.append(child)
					self.frontier_check.add(string_state) #append to right side of dependent on search method
		print "No solution found, too bad!"

	def DFS(self):
		self.frontier = deque() #Add first node to the right side of the deque
		self.frontier.append(self.initialstate)
		while self.frontier: # DIT MOET EEN PRIORITY QUEUE WORDEN IPV NORMALE QUEUE
			if len(self.frontier) > self.max_fringe_size:
				self.max_fringe_size = len(self.frontier)
			current_node = self.frontier.pop() #ZAL AUTOMATISCH DE NODE POPPEN DIE LAAGSTE WAARDE
			
			length = len(current_node.state)
			n = math.sqrt(length)

			if self.is_goal_node(current_node) == True: #2.retrive information and create output fill in seperate method of solver class
				print "Solution found"
				self.write_output_file(current_node)
				self.visited.add(tuple(current_node.state)) #append current node to visited
				exit()

			self.get_children(current_node, n)	#3.expand node and put in frontier only if not yet in frontier and not yet visited     
			# kan dit in een aparte method?
			self.nodes_expanded += 1
			if current_node.search_depth > self.max_search_depth:
				self.max_search_depth = current_node.search_depth
			
			visited_state = ','.join(str(v) for v in current_node.state)#append current node to visited
			self.visited.add(visited_state)
			
			while len(current_node.childs) > 0:
				child = current_node.childs.pop()
				string_state = ','.join(str(v) for v in child.state)
				if string_state not in self.visited and string_state not in self.frontier_check:    #not any(x.state == child.state for x in self.frontier): 
					self.frontier.append(child)
					self.frontier_check.add(string_state) #append to right side of dependent on search method
			#pdb.set_trace()
		print "No solution found, too bad!"

	def AST(self):

		#ADD ROOT NODE WITH HEURISTIC
		self.heuristic(self.initialstate)
		self.frontier = [(self.initialstate.heuristic,self.initialstate)]

		while self.frontier:
			if len(self.frontier) > self.max_fringe_size:
				self.max_fringe_size = len(self.frontier)

			#REMOVE NODE FROM FRONTIER
			cost,current_node = heapq.heappop(self.frontier)

			length = len(current_node.state)
			n = math.sqrt(length)

			#CHECK IF NODE IS GOAL NODE
			if self.is_goal_node(current_node) == True: 
				print "Solution found"
				self.write_output_file(current_node)
				self.visited.add(tuple(current_node.state))
				exit()

			#EXPAND CURRENT NODE
			self.get_children(current_node, n)	
			self.nodes_expanded += 1
			if current_node.search_depth > self.max_search_depth:
				self.max_search_depth = current_node.search_depth
			
			visited_state = ','.join(str(v) for v in current_node.state) #append current node to visited in string format
			self.visited.add(visited_state)
			
			#ADD NODES TO FRONTIER
			while len(current_node.childs) > 0:
				child = current_node.childs.popleft()
				string_state = ','.join(str(v) for v in child.state)
				if string_state not in self.visited and string_state not in self.frontier_check: 
					self.heuristic(child)
					heapq.heappush(self.frontier,(child.heuristic,child))
					self.frontier_check.add(string_state) #append to right side of dependent on search method
		print "No solution found, too bad!"

	def IDA(self):
		bound = 0
		while bound >= 0:
			#ADD ROOT NODE WITH HEURISTIC
			self.heuristic(self.initialstate)
			self.frontier = [(self.initialstate.heuristic,self.initialstate)]

			print "THIS IS FRONTIER NOW"
			for cost, node in self.frontier:
				
				print node.state
				print cost
	
			while self.frontier:
				if len(self.frontier) > self.max_fringe_size:
					self.max_fringe_size = len(self.frontier)
	
				#REMOVE NODE FROM FRONTIER
				cost,current_node = heapq.heappop(self.frontier)
	
				length = len(current_node.state)
				n = math.sqrt(length)

				print "THIS IS CURRENT NODE"
				print current_node.state
	
				#CHECK IF NODE IS GOAL NODE
				if self.is_goal_node(current_node) == True: 
					print "Solution found"
					self.write_output_file(current_node)
					self.visited.add(tuple(current_node.state))
					exit()
	
				#EXPAND CURRENT NODE
				self.get_children(current_node, n)	
				self.nodes_expanded += 1
				if current_node.search_depth > self.max_search_depth:
					self.max_search_depth = current_node.search_depth
				
				visited_state = ','.join(str(v) for v in current_node.state) #append current node to visited in string format
				self.visited.add(visited_state)
				
				#ADD NODES TO FRONTIER
				while len(current_node.childs) > 0:
					child = current_node.childs.popleft()
					string_state = ','.join(str(v) for v in child.state)
					if string_state not in self.visited and string_state not in self.frontier_check:
						if not child.search_depth > bound:
							self.heuristic(child)
							heapq.heappush(self.frontier,(child.heuristic,child))
							self.frontier_check.add(string_state) #append to right side of dependent on search method
				bound += 1
			
		print "No solution found, too bad!"

	def is_goal_node(self, current_node):
		goalstate = list(current_node.state) #sort numbers to goalstate
		goalstate.sort()
		if current_node.state == goalstate:
			return True

	def get_children(self, current_node, n):
		index = int(current_node.state.index(0))
		n = int(n)
		if not index < n: #UP
			up_child = State(list(current_node.state), parent_node  = current_node, node_type = 'Up')
			up_child.state[index], up_child.state[index-n]  = up_child.state[index-n], up_child.state[index]
			up_child.search_depth = current_node.search_depth + 1
			current_node.childs.append(up_child)
		if not index >= (n*n)-n : #DOWN
			down_child = State(list(current_node.state), parent_node  = current_node, node_type = 'Down')#create new node with same as current state, switch state in next line
			down_child.state[index], down_child.state[index+n]  = down_child.state[index+n], down_child.state[index]
			down_child.search_depth = current_node.search_depth + 1
			current_node.childs.append(down_child) #append child node to current node childs
		if not index %n == 0: #LEFT
			left_child = State(list(current_node.state), parent_node  = current_node, node_type = 'Left') #create new node with same as current state, switch state in next line
			left_child.state[index], left_child.state[index-1]  = left_child.state[index-1], left_child.state[index]
			left_child.search_depth = current_node.search_depth + 1
			current_node.childs.append(left_child)
		if not (index +1) %n == 0: #RIGHT
			right_child = State(list(current_node.state), parent_node  = current_node, node_type = 'Right') #create new node with same as current state, switch state in next line
			right_child.state[index], right_child.state[index+1]  = right_child.state[index+1], right_child.state[index]
			right_child.search_depth = current_node.search_depth + 1
			current_node.childs.append(right_child)
		return current_node

	def col_num(self, index):
		return (index % self.size) +1
	def row_num(self, index):
		return (index / self.size) +1

	def heuristic(self, child):
		h = 0		
		for index, number in enumerate(child.state):
			if number is not 0:
				goal_col = self.col_num(number)
				goal_row = self.row_num(number)
				col = self.col_num(index)
				row = self.row_num(index)
				steps = abs(goal_col-col)+abs(goal_row-row)
				h = h + steps
				child.heuristic = h + child.search_depth
		return child

	def retrive_path(self, current_node):
		sys.setrecursionlimit(1500)
		if current_node.parent_node > 0:
			self.path_to_goal.insert(0, current_node.node_type)
			recursive_node = current_node.parent_node
			self.retrive_path(recursive_node)

	def write_output_file(self, current_node):
		running_time = time.time() - self.start_time
		running_time = round(running_time, 8)
		self.search_depth = current_node.search_depth
		self.cost_of_path = self.search_depth
		self.retrive_path(current_node)
		self.fringe_size = len(self.frontier)
		self.max_ram_usage = float(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)/1000000
		self.max_ram_usage = round(self.max_ram_usage, 8)
		text_file = open('output.txt', 'w+')
		text_file.write("path_to_goal: %s \ncost_of_path: %s \nnodes_expanded: %s \nfringe_size: %s \nmax_fringe_size: %s \nsearch_depth: %s \nmax_search_depth: %s \nrunning_time: %s \nmax_ram_usage: %s" % (self.path_to_goal, self.cost_of_path, self.nodes_expanded, self.fringe_size, self.max_fringe_size, self.search_depth, self.max_search_depth, running_time, self.max_ram_usage))
		text_file.close()
